# BC-pi
##### Día a día podemos ver como la implementación de las bicicletas se va abriendo paso entre las calles, dadas sus características eco amigables se han vuelto un atractivo notable para la juventud consciente de la contaminación que generan los automóviles. Según la Encuesta Nacional de Medio Ambiente, cuyos resultados fueron presentados en febrero de 2018, el 8% de la población que reside en la Región Metropolitana utiliza la bicicleta como principal medio de transporte. Sin embargo, hay que tener consideración con los accidentes que estas pueden provocar; según la CONASET el 5% de los fallecidos en el tránsito son ciclistas y solo durante el año 2019, la bicicleta participó en 3.840 siniestros viales y resultaron 85 ciclistas fallecidos y 3.199 lesionados.
##### Es por esta razón que decidimos crear BC-pi, un aparato que por medio de sensores de ultrasonido mide la distancia de la bicicleta con respecto a medios de transporte cercanos que se encuentren dentro de una distancia mínima lateral de 1,5 metros tras el/la conductor/a de la bicicleta. Si están dentro de este rango, una serie de luces que se encontrarán en el manubrio se encenderán a modo de alerta para el ciclista.

##### Realizado por:
- Máximo Flores: 202130019-8
- Catalina González: 202130010-4
- Andrea Rodríguez: 202173009-5
- Nicolas Verdugo: 202012033-1

##### Datos obtenidos de:
[](url)https://www.pauta.cl/nacional/cada-ano-hay-mas-ciclistas-en-santiago-y-cada-ano-mueren-menos-de-ellos

[](url)https://www.conaset.cl/ciclistas/

##### Se incluye un archivo mp4, en ese video se señala mas información del proyecto.

> # CONTENIDOS
- MATERIALES
- PASOS A SEGUIR
- INSTALACIÓN
- ADVERTENCIAS
- DISEÑO

># MATERIALES
- Sensor de ultrasonido (HC-SR04) = $ 1.290
[(https://www.mcielectronics.cl/shop/product/sensor-de-proximidad-de-ultrasonido-hc-sr04-23582)](url)
- Luces led = $500 x 4= $1000(apróx.)
- Raspberry (Incluido en el kit)
- Protoboard (Incluido en el kit)
- 6 Resistencias 220 Ω (Incluidas en el kit)
- Cables jumpers (Incluidos en el kit)
- Cable jumper 5 metros ($200 el metro)
- Case raspberry (Incluido en el kit)
- Cable tipo C (Incluido en el kit)
- Caja de cartón
- Lata de bebida
- Amarras plásticas = $5.790 (50 unidades)(https://www.sodimac.cl/sodimac-cl/product/1578170/Amarra-cables-48x450-mm-50-unidades-Negro/1578170)
 
> # PASOS A SEGUIR
##### PASO 1:
Habiendo iniciado la raspberry, para empezar con este proyecto primero debemos descargar el archivo "luces_y_sensor.py" (que corresponde a la versión final del código) que contiene el código con el objetivo del proyecto: encender luces en el manubrio de acuerdo a un sensor de ultrasonido.

##### PASO 2:
Ahora debemos realizar las conexiones físicas de nuestra BC-pi siguiendo el siguiente diagrama.
Las conexiones de la raspberry a la Protoboard son las siguientes:

![ScreenShot]( DIAGRAMA_DE_CONEXIONES.png)

##### PASO 3:
Luego, necesitaremos crear el arranque directo del código para que al iniciar la raspberry, este inicie automáticamente. Para esto primero hay que poner el archivo "luces_y_sensor" en el escritorio de la raspberry y en el cmd escribimos el comando: 
```shell
 sudo crontab -e
```
Después de esto pueden aparecer 3 opciones y seleccionar la opción donde aparece "nano", o bien ya viene predeterminado con este. Luego, hay que ir a la última parte del código, después de la última línea escribimos el siguiente comando en una línea nueva: 
``` shell
 @reboot sudo python3 /home/pi/Desktop/luces_y_sensor.py
```
Luego con:
``` shell
 ctrl + o
``` 
se guarda, apretamos:
``` shell
 Enter
``` 
y luego:
``` shell
 ctrl + x
```
Finalmente, en el menú de la raspberry vamos a reboot y esperamos, pero también podemos en el mismo cmd escribir el comando: 
```shell
 sudo reboot now
```
y así se reiniciaría igualmente al presionar:
``` shell
 Enter
```
Después de esperar unos minutos debería empezar el programa.  

Después de haber concluido con el paso 3 podemos empezar con el proceso de instalación.

> # INSTALACIÓN
#####   Para implementarlo en la bicicleta necesitamos de una caja que contenga a la raspberry con la Protoboard. Para esto nosotros utilizaremos la base del case que viene con el kit dentro de una caja más grande de cartón, haciéndole perforaciones para que los cables de salida (el cable de unión con las luces led y el cable que conecta con el cargador portátil) y el sensor tengan contacto con el exterior. Esta caja estará situada bajo el asiento de la bicicleta y sobre esta estará el cargador portátil que alimentará la raspberry. Para mantener la caja en su lugar, usaremos amarras plásticas.
#####   Para la parte delantera donde se ubicarán las luces vamos a usar la mitad inferior de una lata que estará perforada para que entren las luces led, de este modo la apertura de la lata va a dirigirse hacia el/la conductor/a reflejando la luz en el interior de aluminio. Esto va unido al manubrio por medio de amarras plásticas y a lo largo del marco de la bicicleta va el cable jumper de 5 metros que conecta las luces con la raspberry.

> # DISEÑO
![ScreenShot]( FOTO_1.png)
![ScreenShot]( FOTO_2.png)
![ScreenShot]( FOTO_3.png)
##### El diseño en 3D se encuentra bajo el nombre "prototype.glb" en un archivo de paint 3D.
Además añadimos un archivo RAR con el nombre "EJEMPLOS" el cual contiene distintas fotografías de las versiones de diseño realizada por los integrantes del grupo.
> # ADVERTENCIAS
##### Cuando realizamos el prototipo se produjeron las siguientes complicaciones:
- En el kit vienen cables macho-macho y para transformarlos a hembra seguimos las instrucciones del siguiente video: https://www.youtube.com/watch?v=qNFRg2yv_IY
- Si no logramos conseguir un cable jumper del largo necesario, podemos usar un cable normal y soldamos las puntas para crear uno más largo. El diámetro del cable debe ser de diámetro igual o superior al del jumper.
- Al conectar el cargador portátil (fuente de energía) demora un poco en activarse, sin embargo, se logra activar.
##### Señalamos esto para que se realicen con cuidado estas acciones.
